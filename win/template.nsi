SetCompressor /SOLID lzma

!define PRODUCT_PUBLISHER "LEAP Encryption Access Project"
!include "MUI2.nsh"

Name "$app_name"
Outfile "..\dist\$app_name-$version.exe"
;TODO make the installdir configurable - and set it in the registry.
InstallDir "C:\Program Files\$app_name\"
RequestExecutionLevel admin

!include FileFunc.nsh
!insertmacro GetParameters
!insertmacro GetOptions

;Macros

!macro SelectByParameter SECT PARAMETER DEFAULT
	${GetOptions} $R0 "/${PARAMETER}=" $0
	${If} ${DEFAULT} == 0
		${If} $0 == 1
			!insertmacro SelectSection ${SECT}
		${EndIf}
	${Else}
		${If} $0 != 0
			!insertmacro SelectSection ${SECT}
		${EndIf}
	${EndIf}
!macroend



!define BITMAP_FILE riseupvpn.bmp

!define MUI_ICON "..\assets\$app_name_lower.ico"
!define MUI_UNICON "..\assets\$app_name_lower.ico"

!define MUI_WELCOMEPAGE_TITLE "$app_name"
!define MUI_WELCOMEPAGE_TEXT "This will install $app_name in your computer. $app_name is a simple, fast and secure VPN Client, powered by Bitmask. \n This VPN service is run by donations from people like you."
#!define MUI_WELCOMEFINISHPAGE_BITMAP "riseup.png"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
 
 

Section "InstallFiles"
  ; first we try to delete the systray, locked by the app.
  ClearErrors
  Delete 'C:\Program Files\$app_name\bitmask-vpn.exe'
  IfErrors 0 noError

  ; Error handling
  MessageBox MB_OK|MB_ICONEXCLAMATION "$app_name is Running. Please close it, and then run this installer again."
  Abort

  noError:
  ExecShellWait "runas" "$INSTDIR\nssm.exe" 'stop $app_name_lower-helper'
  ExecShellWait "runas" "$INSTDIR\nssm.exe" 'remove $app_name_lower-helper confirm'

  SetOutPath $INSTDIR 
  WriteUninstaller $INSTDIR\uninstall.exe

  ; Add ourselves to Add/remove programs
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower" "DisplayName" "$app_name"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower" "InstallLocation" "$INSTDIR"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower" "DisplayIcon" "$INSTDIR\icon.ico"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower" "Readme" "$INSTDIR\readme.txt"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower" "DisplayVersion" "$version"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower" "Publisher" "LEAP Encryption Access Project"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower" "NoRepair" 1

  ;Start Menu
  createDirectory "$SMPROGRAMS\$app_name\"
  createShortCut "$SMPROGRAMS\$app_name\$app_name.lnk" "$INSTDIR\bitmask-vpn.exe" "" "$INSTDIR\icon.ico"

  File "readme.txt"
  File "..\staging\nssm.exe"
  File "/oname=icon.ico" "..\assets\$app_name_lower.ico"

  $extra_install_files

SectionEnd

Section "InstallService"
  ; Easy service management thanks to nssm
  ExecWait '"$INSTDIR\nssm.exe" install $app_name_lower-helper "$INSTDIR\bitmask_helper.exe"'
  ExecWait '"$INSTDIR\nssm.exe" set $app_name_lower-helper AppDirectory "$INSTDIR"'
  ExecWait '"$INSTDIR\nssm.exe" start $app_name_lower-helper'
SectionEnd

Section /o "TAP Virtual Ethernet Adapter" SecTAP
	; Adapted from the windows nsis installer from OpenVPN (openvpn-build repo).
	DetailPrint "Installing TAP (may need confirmation)..."
	; The /S flag make it "silent", remove it if you want it explicit
  	ExecWait '"$INSTDIR\tap-windows.exe" /S /SELECT_UTILITIES=1'
	Pop $R0 # return value/error/timeout
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$app_name" "tap" "installed"
	DetailPrint "TAP installed!"
SectionEnd

Section "Uninstall"
  ExecShellWait "runas" "$INSTDIR\nssm.exe" 'stop $app_name_lower-helper'
  ExecShellWait "runas" "$INSTDIR\nssm.exe" 'remove $app_name_lower-helper confirm'

  Delete $INSTDIR\readme.txt
  Delete $INSTDIR\nssm.exe
  Delete $INSTDIR\helper.log
  Delete "$SMPROGRAMS\$app_name\$app_name.lnk"
  RMDir "$SMPROGRAMS\$app_name\"

  $extra_uninstall_files

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$app_name_lower"
  ; uninstaller must be always the last thing to remove
  Delete $INSTDIR\uninstall.exe
  RMDir $INSTDIR
SectionEnd

Function .onInit
	!insertmacro SelectByParameter ${SecTAP} SELECT_TAP 1
FunctionEnd

;----------------------------------------
;Languages
 
!insertmacro MUI_LANGUAGE "English"
